import React from 'react';
import {connect} from 'react-redux';
import {searchAlbums} from '../actions/search';

class SearchBar extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            searchTerm: '',
            formErrors: {
                search: []
            }
        };
    }

    render() {

        let searchBarErrors = null;
        let searchBarInputClasses = ["form-control"];

        if(this.state.formErrors.length > 0) {
            searchBarErrors = this.state.formErrors.map(error => (
                <div>
                    {error}
                </div>
            ));

            searchBarInputClasses.push("is-invalid");
        }

        return (
            <div id="search-bar" className="row">
                <div className="col-12 mt-3 mb-3">
                    <form onSubmit={this.submit.bind(this)} noValidate>
                        <div className="form-group">
                            <label htmlFor="artistName">
                                Search for music albums
                            </label>
                            <div className="input-group">
                                <input 
                                    id="artistName" 
                                    type="text"
                                    placeholder="Enter artist or album name"
                                    className={searchBarInputClasses.join(' ')}
                                    value={this.state.value} 
                                    onChange={this.handleInputChange.bind(this)}
                                    required
                                />
                                <div className="input-group-append">
                                    <button type="submit" className="btn btn-outline-secondary">Submit</button>
                                </div>
                                <div className="invalid-feedback">
                                    {searchBarErrors}
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }

    handleInputChange(event) {
        this.setState({searchTerm: event.target.value});
    }

    //leverage the submit functionality for pressing enter or a button,
    //but override it with custom functionality
    submit(event) {
        event.preventDefault();

        if(this.validate()) {
            this.props.searchForAlbums(this.state.searchTerm)
        }
    }

    validate() {
        if(!this.state.searchTerm) {
            this.setState({formErrors: ['Please enter search term']});
            return false;
        }

        this.setState({formErrors: []});
        return true;
    }
}

const mapStateToProps = state => {
    return {
        term: state.search.term,
        isFetching: state.search.isFetching
    };
};

const mapDispatchToProps = dispatch => {
    return {
        searchForAlbums: (term) => dispatch(searchAlbums(term))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);