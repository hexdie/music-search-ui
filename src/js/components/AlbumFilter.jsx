import React from 'react';
import {connect} from 'react-redux';
import {filterAlbums} from '../actions/search';

class AlbumFilter extends React.Component {
    
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <label htmlFor="search-result-filter">
                    Album Filter
                </label>
                <div className="input-group">
                    <input 
                        id="search-result-filter"
                        type="text" 
                        className="form-control"
                        onChange={this.updateFilterTerm.bind(this)} 
                        placeholder="Filter results by album name"
                    />
                </div>
            </div>
        )
    }

    updateFilterTerm(event) {
        this.props.filter(event.target.value);
    }
}

const mapStateToProps = state => {
    return {
        filterTerm: state.search.filterTerm
    }
};

const mapDispatchToProps = dispatch => {
    return {
        filter: (term) => dispatch(filterAlbums(term))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AlbumFilter);