import React from 'react';
import {connect} from 'react-redux';
import AlbumFilter from './AlbumFilter';

class SearchResults extends React.Component {
    
    constructor(props) {
        super(props);

        this.state = {
            height: window.innerHeight,
            width: window.innerWidth
        };
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleResize.bind(this));
    }

    handleResize() {
        this.setState({
            height: window.innerHeight,
            width: window.innerWidth
        });
    }

    render() {

        if(this.props.isFetching) {
            return (
                <div className="loader-container">
                    <div className="loader"></div>
                </div>
            );
        }

        if(this.props.searchError) {
            return (
                <div className="row">
                    <div className="col-12">
                        <div className="alert alert-danger">
                            An error has occurred while trying to search. Please try again.
                        </div>
                    </div>                    
                </div>
            );
        }

        let searchResults = null;
        let searchResultsFound = true;
        let resultsFilter = null;
        let useLargeImages = window.matchMedia('(min-width: 500px)').matches;

        if(this.props.results) {

            if(this.props.results.length > 0) {
                //map each search result into its own row
                searchResults = this.props.results.filter(r => r.visible).map((r, i) => (
                    <div className="row mt-3 mb-3" key={`search-result-${i}`}>
                        <div className="col-3 col-sm-3 col-md-2">
                            <img src={useLargeImages ? r.artwork100 : r.artwork60} />
                        </div>
                        <div className="col-3 col-sm-4 col-md-3">
                            {r.artist}
                        </div>
                        <div className="col-6 col-sm-5 col-md-7">
                            {r.collection}
                        </div>
                    </div>
                ));

                //add header row
                searchResults.unshift((
                    <div className="row mt-3 mb-3 pt-2 pb-2 results-table-header" key="results-table-header">
                        <div className="col-3 col-sm-3 col-md-2"></div>
                        <div className="col-3 col-sm-4 col-md-3">
                            Artist
                        </div>
                        <div className="col-6 col-sm-5 col-md-7">
                            Album
                        </div>
                    </div>
                ));
            }
            else {
                searchResults = (
                    <div className="pt-3">
                        No results found
                    </div>
                );

                searchResultsFound = false;
            }

            if(searchResultsFound) {
                resultsFilter = (
                    <div className="results-filter">
                        <div className="row">
                            <div className="col-12">
                                <AlbumFilter />
                            </div>
                        </div>
                    </div>
                );
            }

            return (
                <div id="search-results mt-3">
                    {resultsFilter}
                    <div className="row">
                        <div className="col-12">
                            {searchResults}
                        </div>
                    </div>
                </div>
            );
        }

        return null;
    }
}

const mapStateToProps = state => {
    return {
        searchTerm: state.search.searchTerm,
        results: state.search.results,
        searchError: state.search.hasErrored,
        isFetching: state.search.isFetching
    };
};

export default connect(mapStateToProps)(SearchResults);