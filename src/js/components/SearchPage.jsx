import React from 'react';
import SearchBar from './SearchBar';
import SearchResults from './SearchResults';

export default class SearchPage extends React.Component {
    render() {
        return (
            <div id="search-page">
                <div className="row">
                    <div className="col-12">
                        <h1 className="mt-3">
                            Music Album Search
                        </h1>
                    </div>
                </div>
                <SearchBar />
                <SearchResults />
            </div>
        );
    }
}