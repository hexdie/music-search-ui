import React from 'react';
import {Route} from 'react-router-dom'
import SearchPage from './SearchPage';

export default class App extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="container pb-3">
                <Route path="/" exact component={SearchPage} />
            </div>
        );
    }

}