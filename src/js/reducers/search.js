import {
    SEARCH_ERROR,
    SEARCH_FETCHING,
    SEARCH_FILTER_UPDATE,
    SEARCH_SUBMIT,
    SEARCH_SUCCESS
} from '../actions/types';

const initialState = {
    filterTerm: '',
    hasErrored: false,
    isFetching: false,
    results: null,
    searchTerm: ''
};

const searchSubmit = (state, action) => {
    return {
        ...state,
        searchTerm: action.term
    };
}

const searchFetching = (state, action) => {
    return {
        ...state,
        isFetching: true
    };
}

const searchSuccess = (state, action) => {

    let searchResults = action.results.map(result => ({
        ...result,
        visible: true
    }));

    //sort albums alphabetically
    searchResults.sort((a, b) => {
        return a.collection.localeCompare(b.collection);
    });

    return {
        ...state,
        isFetching: false,
        hasErrored: false,
        results: searchResults
    };
}

const searchError = (state, action) => {
    return {
        ...state,
        isFetching: false,
        hasErrored: true
    }
}

const searchFilterUpdate = (state, action) => {

    //filter albums by search terms, setting results as visible or not visible
    let filteredResults = state.results.map(result => {
        if(result.collection.toLowerCase().includes(action.term.toLowerCase())) {
            result.visible = true;
        }
        else {
            result.visible = false;
        }

        return result;
    });

    return {
        ...state,
        filterTerm: action.term,
        results: filteredResults
    };
}

//create a reducer from an initial state and
//a mapping of action types to functions that update the state
function createReducer(initialState, handlers) {
    return function reducer(state = initialState, action) {
        if (handlers.hasOwnProperty(action.type)) {
            return handlers[action.type](state, action);
        }
        else {
            return state;
        }
    }
}

export default createReducer(initialState, {
    [SEARCH_ERROR]: searchError,
    [SEARCH_FETCHING]: searchFetching,
    [SEARCH_FILTER_UPDATE]: searchFilterUpdate,
    [SEARCH_SUBMIT]: searchSubmit,
    [SEARCH_SUCCESS]: searchSuccess
});