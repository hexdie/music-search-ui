import {
    SEARCH_ERROR,
    SEARCH_FETCHING,
    SEARCH_FILTER_UPDATE,
    SEARCH_SUBMIT,
    SEARCH_SUCCESS
} from './types';

import searchService from '../service/search';

export const searchAlbums = term => dispatch => {
    
    dispatch({ type: SEARCH_SUBMIT, term });
    dispatch({ type: SEARCH_FETCHING });

    searchService.searchAlbums(term)
        .then(results => {
            dispatch({
                type: SEARCH_SUCCESS,
                results: results.data.map(result => ({
                    artist: result.artist,
                    artistId: result.artistId,
                    artwork60: result.artwork60,
                    artwork100: result.artwork100,
                    collection: result.collection,
                    collectionId: result.collectionId
                }))
            });
        })
        .catch(error => {
            dispatch({
                type: SEARCH_ERROR
            });

            console.error(error);
        });
}

export const filterAlbums = term => {
    return {
        type: SEARCH_FILTER_UPDATE,
        term
    };
};