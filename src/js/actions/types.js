export const SEARCH_FILTER_UPDATE = 'FILTER_UPDATE';
export const SEARCH_ERROR = 'SEARCH_ERROR';
export const SEARCH_FETCHING = 'SEARCH_FETCHING';
export const SEARCH_SUBMIT = 'SEARCH_SUBMIT';
export const SEARCH_SUCCESS = 'SEARCH_SUCCESS';