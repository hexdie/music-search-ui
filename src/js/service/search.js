import axios from 'axios';

const searchAlbums = term => {
    return axios.get(`/api/albums/${term}`);
};

export default {
    searchAlbums
};