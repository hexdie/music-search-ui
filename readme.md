# Music Album Search UI

UI for an music album search application

## Local Setup

1. Install node if not already installed: https://nodejs.org/en/
2. Clone this repository locally
3. In the local repository directory
    - run `npm install`
    - run `npm start`
4. Open up `http://localhost:3030` in your web browser
