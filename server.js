const express = require('express');
const proxy = require('express-http-proxy');

const app = express();

app.use(express.static('dist'));

app.use('/api', proxy('http://localhost:3031'));

app.listen('3030', () => console.log('music-search-ui running on 3030'));